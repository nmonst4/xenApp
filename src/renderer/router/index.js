import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPageView').default
    },
    {
      path: '/server/:serverid',
      name: 'landing-page-server',
      component: require('@/components/LandingPageView').default
    },
    {
      path: '/vm/:serverid/:uuid',
      name: 'landing-page-vm',
      component: require('@/components/LandingPageView').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
