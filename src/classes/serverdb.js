const electron = require('electron')
const path = require('path')
const CryptoJS = require('crypto-js')

export default class ServerDB {
  constructor () {
    // create database connection
    this.documentPath = (electron.app || electron.remote.app).getPath('documents')
    this.serverDBfile = path.join(this.documentPath, '.xenapp_server.nosql')
    this.NoSQL = require('nosql')
    this.db = this.NoSQL.load(this.serverDBfile)
    this.sec = 'buIzonyXcsw316wM7l4C7CExiJmYJRce'
  }

  addServer (data, callback) {
    data.id = require('shortid').generate()
    data.password = this.encrypt(data.password)
    this.db.insert(data).callback(function (err) {
      if (err) return callback(err)
      callback()
    })
  }

  removeServer (id, callback) {
    this.db.remove().make(function (builder) {
      builder.first()
      builder.where('id', id)
      builder.callback(function (err, count) {
        if (err) return callback(err)
        callback()
      })
    })
  }

  getAllServer (callback) {
    this.db.find().make(function (builder) {
      builder.callback(function (err, response) {
        if (err) return callback(err)
        callback(response)
      })
    })
  }

  getServerByID (id, callback) {
    this.db.find().make(function (builder) {
      builder.first()
      builder.where('id', id)
      builder.callback(function (err, response) {
        if (err) return callback(err)
        callback(response)
      })
    })
  }

  encrypt (string) {
    return CryptoJS.AES.encrypt(string, this.sec).toString()
  }

  decrypt (text) {
    let bytes = CryptoJS.AES.decrypt(text, this.sec)
    return bytes.toString(CryptoJS.enc.Utf8)
  }
}
