import XenApi from './xenapi'

export default class XenServer extends XenApi {
  constructor (servername, username, password, callback) {
    super(servername, username, password)
    super.log('XenServer loaded')
    let parent = this
    this.getSessionbyName(servername, function (sessionRef) {
      // console.log(sessionRef)
      callback(sessionRef)
      parent.disconnect()
    })
  }

  getSessionbyName (name, callback) {
    let self = this
    self.xapi.connect().then(function () {
      self.xapi.call('host.get_by_name_label', name).then(function (sessionRef) {
        callback(sessionRef[0])
      })
    }).catch(error => {
      callback(error)
    })
  }

  getIP (sessionref, callback) {
    let self = this
    self.xapi.call('host.get_address', sessionref).then(function (ip) {
      callback(ip)
    }).catch(error => {
      callback(error)
    })
  }

  getXenVersion (sessionref, callback) {
    let self = this
    self.xapi.call('host.get_software_version', sessionref).then(function (version) {
      callback(version.product_version)
    }).catch(error => {
      callback(error)
    })
  }
}
