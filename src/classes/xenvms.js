import XenApi from './xenapi'

export default class XenVms extends XenApi {
  constructor (uuid, servername, username, password, callback) {
    super(servername, username, password)
    super.log('Xen VMS')
    let parent = this
    if (uuid) {
      this.getSessionbyUUID(uuid, function (sessionRef) {
        // console.log(sessionRef)
        callback(sessionRef)
        parent.disconnect()
      })
    }
  }

  getSessionbyUUID (uuid, callback) {
    let self = this
    self.xapi.connect().then(function () {
      self.xapi.call('VM.get_by_uuid', uuid).then(function (sessionRef) {
        callback(sessionRef)
      })
    }).catch(error => {
      callback(error)
    })
  }

  getAllVms (callback) {
    let self = this

    // Force connection.
    self.xapi.connect().then(function () {
      self.xapi.call('VM.get_all_records').then(function (vms) {
        let vmsArr = []
        for (let key in vms) {
          // skip loop if the property is from prototype
          if (!vms.hasOwnProperty(key)) continue

          if (!vms[key].is_a_template && !vms[key].is_control_domain) {
            vmsArr.push(vms[key])
          }
        }
        callback(vmsArr)
      })
    }).catch(error => {
      callback(error)
    })
  }

  getVMName (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.get_name_label', sessionRef).then(function (name) {
      callback(name)
    }).catch(error => {
      callback(error)
    })
  }

  getPowerState (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.get_power_state', sessionRef).then(function (powerstate) {
      callback(powerstate)
    }).catch(error => {
      callback(error)
    })
  }

  /**
   *  need tests
   *
   * @param sessionRef
   * @param callback
   */
  rebootVM (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.hard_reboot', sessionRef).then(function (res) {
      callback(res)
    }).catch(error => {
      callback(error)
    })
  }

  /**
   * is working
   *
   * @param sessionRef
   * @param callback
   */
  shutdownVM (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.shutdown', sessionRef).then(function (res) {
      callback(res)
    }).catch(error => {
      callback(error)
    })
  }

  /**
   * is working
   *
   * @param sessionRef
   * @param callback
   */
  startVM (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.start', sessionRef, false, true).then(function (res) {
      callback(res)
    }).catch(error => {
      callback(error)
    })
  }

  suspendVM (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.suspend', sessionRef).then(function (res) {
      callback(res)
    }).catch(error => {
      callback(error)
    })
  }

  resumeVM (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.resume', sessionRef, false, true).then(function (res) {
      callback(res)
    }).catch(error => {
      callback(error)
    })
  }

  pauseVM (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.pause', sessionRef).then(function (res) {
      callback(res)
    }).catch(error => {
      callback(error)
    })
  }

  unpauseVM (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.unpause', sessionRef).then(function (res) {
      callback(res)
    }).catch(error => {
      callback(error)
    })
  }

  getConsoleRef (sessionRef, callback) {
    let self = this
    self.xapi.call('VM.get_consoles', sessionRef).then(function (res) {
      callback(res)
    }).catch(error => {
      callback(error)
    })
  }
}
