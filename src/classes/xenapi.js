export default class XenApi {
  // Documentation: http://xapi-project.github.io/xen-api/
  constructor (servername, username, password) {
    this.servername = servername
    this.readOnly = false
    this.xapi = require('xen-api').createClient({
      url: 'https://' + servername,
      auth: {
        user: username,
        password: password
      },
      readOnly: this.readOnly,
      allowUnauthorized: true
    })
  }

  testConnection (callback) {
    let self = this
    // Force connection.
    this.xapi.connect().then(function () {
      self.xapi.call('network.get_all_records').then(function (network) {
        // console.log(network)
        callback()
        self.disconnect()
      })
    }).catch(error => {
      callback(error)
    })
  }

  log (msg) {
    console.log('XenApi: ', msg)
  }

  disconnect () {
    this.xapi.disconnect()
  }
}
