import XenApi from './xenapi'

export default class XenConsole extends XenApi {
  constructor (uuid, servername, username, password, callback) {
    super(servername, username, password)
    super.log('Xen Console')
    let parent = this
    if (uuid) {
      this.getSessionbyUUID(uuid, function (sessionRef) {
        // console.log(sessionRef)
        callback(sessionRef)
        parent.disconnect()
      })
    }
    callback()
  }

  getSessionbyUUID (uuid, callback) {
    let self = this
    self.xapi.connect().then(function () {
      self.xapi.call('console.get_by_uuid', uuid).then(function (sessionRef) {
        callback(sessionRef)
      })
    }).catch(error => {
      console.error(error)
    })
  }

  getUUID (sessionRef, callback) {
    console.warn(sessionRef)
    let self = this
    self.xapi.connect().then(function () {
      self.xapi.call('console.get_uuid', sessionRef).then(function (uuid) {
        callback(uuid)
      })
    }).catch(error => {
      console.error(error)
    })
  }

  getLocation (sessionRef, callback) {
    console.warn(sessionRef)
    let self = this
    self.xapi.connect().then(function () {
      self.xapi.call('console.get_location', sessionRef).then(function (location) {
        callback(location)
      })
    }).catch(error => {
      console.error(error)
    })
  }

  getOtherConfig (sessionRef, callback) {
    console.warn(sessionRef)
    let self = this
    self.xapi.connect().then(function () {
      self.xapi.call('console.get_other_config', sessionRef).then(function (res) {
        callback(res)
      })
    }).catch(error => {
      console.error(error)
    })
  }
}
